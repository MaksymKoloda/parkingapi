﻿using System;
using Parking.Entities;

namespace Parking.Dtos
{
    public class VehicleResponseDto
    {
        public Guid Id { get; set; }

        public double Balance { get; set; }

        public VehicleType Type { get; set; }
    }
}