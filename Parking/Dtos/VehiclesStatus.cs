﻿namespace Parking.Dtos
{
    public class VehiclesStatus
    {
        public int Taken { get; set; }

        public int Free { get; set; }
    }
}