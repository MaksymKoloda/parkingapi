﻿using System;

namespace Parking.Dtos
{
    public class TransactionResponseDto
    {
        public Guid Id { get; set; }

        public Guid VehicleId { get; set; }

        public DateTime Time { get; set; }

        public double Bill { get; set; }
    }
}