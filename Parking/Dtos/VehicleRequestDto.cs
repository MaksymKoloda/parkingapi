﻿using Parking.Entities;

namespace Parking.Dtos
{
    public class VehicleRequestDto
    {
        public double Balance { get; set; }

        public VehicleType Type { get; set; }
    }
}
