﻿using System;

namespace Parking.Entities
{
    public class Vehicle
    {
        public Guid Id { get; }

        public double Balance { get; private set; }

        public VehicleType Type { get; }

        public Vehicle(VehicleType type, double balance)
        {
            Balance = balance;
            Type = type;
            Id = Guid.NewGuid();
        }

        public bool CanPay(double bill)
        {
            return (Balance - bill >= 0);
        }

        public void Charge(double bill)
        {
            Balance -= bill;
        }

        public void AddToBalance(double amount)
        {
            if (amount >= 0)
            {
                Balance += amount;
            }
        }

        public override string ToString()
        {
            return $"Id:{Id}; Type:{Type}; Balance:{Balance}";
        }
    }
}