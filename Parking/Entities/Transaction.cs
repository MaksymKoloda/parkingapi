﻿using System;

namespace Parking.Entities
{
    public class Transaction
    {
        public Guid Id { get; }

        public Guid VehicleId { get; }

        public DateTime Time { get; }

        public double Bill { get; }

        public Transaction(Guid vehicleId, double bill)
        {
            VehicleId = vehicleId;
            Bill = bill;
            Id = Guid.NewGuid();
            Time = DateTime.UtcNow;
        }

        public override string ToString() => $"Time:{Time}; Vehicle:{VehicleId}; Bill:{Bill}";
    }
}