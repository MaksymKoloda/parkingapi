﻿using System.Collections.Generic;
using Parking.Entities;

namespace Parking
{
    public static class Configuration
    {
        public static double InitialBalance { get; set; } = 0;
        public static int VehicleCapacity { get; set; } = 10;
        public static double TransactionInterval { get; set; } = 5;
        public static double FineMultiplier { get; set; } = 2.5;

        public static Dictionary<VehicleType, double> Rates { get; set; } = new Dictionary<VehicleType, double>
        {
            {VehicleType.Car, 2},
            {VehicleType.Truck, 5},
            {VehicleType.Bus, 3.5},
            {VehicleType.Motorcycle, 1}
        };
    }
}