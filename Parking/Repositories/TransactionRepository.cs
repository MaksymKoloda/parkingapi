﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Parking.Entities;
using Parking.Interfaces;

namespace Parking.Repositories
{
    public class TransactionRepository : ITransactionRepository
    {
        public double Balance { get; set; } = Configuration.InitialBalance;

        private readonly ConcurrentDictionary<Guid, Transaction> _transactions = new ConcurrentDictionary<Guid, Transaction>();

        public void RemoveRange(IEnumerable<Transaction> transactions)
        {
            foreach (var transaction in transactions)
            {
                _transactions.TryRemove(transaction.Id, out _);
            }
        }

        public void AddTransaction(Transaction transaction)
        {
            _transactions[transaction.Id] = transaction;
        }

        public IEnumerable<Transaction> GetTransactions(Func<Transaction, bool> filter)
        {
            var transactions = _transactions.Values
                .Where(filter)
                .OrderBy(t => t.Time);

            return transactions;
        }
    }
}