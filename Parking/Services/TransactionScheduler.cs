﻿using System;
using Parking.Entities;
using Parking.Interfaces;

namespace Parking.Services
{
    public class TransactionScheduler : ITransactionScheduler
    {
        private readonly ITransactionRepository _transactionRepository;

        public TransactionScheduler(ITransactionRepository transactionRepository)
        {
            _transactionRepository = transactionRepository;
        }

        private void CreateTransaction(Vehicle vehicle)
        {
            var bill = Configuration.Rates[vehicle.Type];
            var canPay = vehicle.CanPay(bill);
            if (!canPay)
            {
                bill *= Configuration.FineMultiplier;
            }

            vehicle.Charge(bill);
            _transactionRepository.Balance += bill;

            var transaction = new Transaction(vehicle.Id, bill);
            _transactionRepository.AddTransaction(transaction);
        }

        public void StartTransactions(Vehicle vehicle, double interval)
        {
            TimerService.AddTask(vehicle.Id, () => CreateTransaction(vehicle), interval);
        }

        public void EndTransactions(Guid vehicleId)
        {
            TimerService.EndTask(vehicleId);
        }
    }
}