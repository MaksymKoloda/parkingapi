﻿using System;
using System.Collections.Generic;
using System.Timers;

namespace Parking.Services
{
    public static class TimerService
    {
        private static readonly Dictionary<Guid, Timer> Timers = new Dictionary<Guid, Timer>();

        public static void AddTask(Guid id, Action action, double interval)
        {
            var timer = new Timer(interval);
            timer.Elapsed += (sender, args) => action();
            timer.Start();

            Timers[id] = timer;
        }

        public static void EndTask(Guid id)
        {
            if (Timers.TryGetValue(id, out var timer))
            {
                timer.Dispose();
                Timers.Remove(id);
            }
        }
    }
}