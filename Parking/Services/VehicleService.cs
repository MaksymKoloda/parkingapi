﻿using System;
using System.Collections.Generic;
using Mapster;
using Parking.Dtos;
using Parking.Entities;
using Parking.Interfaces;
using Parking.Repositories;

namespace Parking.Services
{
    public class VehicleService : IVehicleService
    {
        private readonly IVehicleRepository _vehicleRepository;

        public int Count => _vehicleRepository.Count;

        public VehicleService(IVehicleRepository vehicleRepository)
        {
            _vehicleRepository = vehicleRepository;
        }

        public IEnumerable<VehicleResponseDto> GetVehicles(Func<Vehicle,bool> filter = default)
        {
            var vehicles = _vehicleRepository.GetVehicles();

            var vehicleDtos = vehicles.Adapt<IEnumerable<Vehicle>, IEnumerable<VehicleResponseDto>>();

            return vehicleDtos;
        }

        public VehiclesStatus GetStatus()
        {
            var taken = _vehicleRepository.Count;
            var free = Configuration.VehicleCapacity - taken;

            var status = new VehiclesStatus { Taken = taken, Free = free };
            return status;
        }
        
        public bool AddVehicle(Vehicle vehicle)
        {
            var added = _vehicleRepository.AddVehicle(vehicle);

            return added;
        }

        public void AddToBalance(Guid vehicleId, double amount)
        {
            var vehicle = _vehicleRepository.GetVehicle(vehicleId);

            vehicle.AddToBalance(amount);
        }

        public void RemoveVehicle(Guid vehicleId)
        {
            _vehicleRepository.RemoveVehicle(vehicleId);
        }
    }
}