﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mapster;
using Parking.Dtos;
using Parking.Entities;
using Parking.Interfaces;

namespace Parking.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly ILogService _logService;
        private readonly ITransactionRepository _transactionRepository;

        public TransactionService(ILogService logService, ITransactionRepository transactionRepository)
        {
            _logService = logService;
            _transactionRepository = transactionRepository;

            TimerService.AddTask(Guid.NewGuid(), () =>
            {
                RemoveOutDatedTransactions();
                StartLogging();
            }, TimeSpan.FromMinutes(1).TotalMilliseconds);
        }

        private void StartLogging()
        {
            var logs = _transactionRepository
                .GetTransactions(t => DateTime.UtcNow < t.Time.Add(TimeSpan.FromMinutes(1)))
                .OrderBy(t => t.Time)
                .Select(t => t.ToString());

            _logService.Log(logs);
        }

        private void RemoveOutDatedTransactions()
        {
            var transactionsToRemove =
                _transactionRepository.GetTransactions(t => DateTime.UtcNow >= t.Time.AddMinutes(1));

            _transactionRepository.RemoveRange(transactionsToRemove);
        }

        public double GetBalance(double ticks = default)
        {
            var timeSpan = TimeSpan.FromMilliseconds(ticks);

            var balance = timeSpan == default
                ? _transactionRepository.Balance
                : _transactionRepository.GetTransactions(t => DateTime.UtcNow < t.Time.Add(timeSpan)).Sum(t => t.Bill);

            return balance;
        }

        public IEnumerable<TransactionResponseDto> GetTransactions(double ticks)
        {
            var timeSpan = TimeSpan.FromMilliseconds(ticks);

            var transactions = _transactionRepository.GetTransactions(t => DateTime.UtcNow < t.Time.Add(timeSpan))
                .OrderBy(t => t.Time);

            var transactionDtos = transactions.Adapt<IEnumerable<Transaction>, IEnumerable<TransactionResponseDto>>();
            return transactionDtos;
        }
    }
}