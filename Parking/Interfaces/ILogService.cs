﻿using System.Collections.Generic;

namespace Parking.Interfaces
{
    public interface ILogService
    {
        void Log(IEnumerable<string> logs);

        IEnumerable<string> GetLogs();
    }
}