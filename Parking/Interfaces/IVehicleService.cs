﻿using System;
using System.Collections.Generic;
using Parking.Dtos;
using Parking.Entities;

namespace Parking.Interfaces
{
    public interface IVehicleService
    {
        int Count { get; }

        bool AddVehicle(Vehicle vehicle);

        VehiclesStatus GetStatus();

        void RemoveVehicle(Guid vehicleId);

        IEnumerable<VehicleResponseDto> GetVehicles(Func<Vehicle, bool> filter = default);
        void AddToBalance(Guid vehicleId, double amount);
    }
}