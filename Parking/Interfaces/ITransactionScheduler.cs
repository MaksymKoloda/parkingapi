﻿using System;
using Parking.Entities;

namespace Parking.Interfaces
{
    public interface ITransactionScheduler
    {
        void StartTransactions(Vehicle vehicle, double interval);

        void EndTransactions(Guid vehicleId);
    }
}