﻿using System;
using System.Collections.Generic;
using Parking.Entities;

namespace Parking.Interfaces
{
    public interface ITransactionRepository
    {
        double Balance { get; set; }

        IEnumerable<Transaction> GetTransactions(Func<Transaction, bool> filter);

        void AddTransaction(Transaction transaction);

        void RemoveRange(IEnumerable<Transaction> transactions);
    }
}