﻿using System.Collections.Generic;
using Parking.Dtos;

namespace Parking.Interfaces
{
    public interface ITransactionService
    {
        double GetBalance(double ticks = default);

        IEnumerable<TransactionResponseDto> GetTransactions(double ticks);
    }
}