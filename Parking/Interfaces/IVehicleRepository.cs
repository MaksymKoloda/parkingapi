﻿using System;
using System.Collections.Generic;
using Parking.Entities;

namespace Parking.Interfaces
{
    public interface IVehicleRepository
    {
        int Count { get; }

        Vehicle GetVehicle(Guid vehicleId);

        IEnumerable<Vehicle> GetVehicles(Func<Vehicle, bool> filter = default);

        bool AddVehicle(Vehicle vehicle);

        void RemoveVehicle(Guid vehicleId);
    }
}