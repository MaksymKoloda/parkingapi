﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Parking.Client.ApiInterfaces;
using Parking.Client.Entities;
using RestEase;

namespace Parking.Client
{
    public static class Program
    {
        private static IVehiclesApi _vehiclesClient;
        private static ITransactionsApi _transactionsClient;
        private static ILogsApi _logsClient;
        private static IEnumerable<Vehicle> _vehicles;

        public static async Task Main()
        {
            await Init();

            await Run();
        }

        private static async Task Init()
        {
            const string url = "https://localhost:5001/api/";
            _vehiclesClient = RestClient.For<IVehiclesApi>(url + "vehicles");
            _transactionsClient = RestClient.For<ITransactionsApi>(url + "transactions");
            _logsClient = RestClient.For<ILogsApi>(url + "logs");

            await _vehiclesClient.Create(new Vehicle { Type = VehicleType.Car, Balance = 40 });
            await _vehiclesClient.Create(new Vehicle { Type = VehicleType.Motorcycle, Balance = 30 });
            await _vehiclesClient.Create(new Vehicle { Type = VehicleType.Bus, Balance = 60 });
            await _vehiclesClient.Create(new Vehicle { Type = VehicleType.Truck, Balance = 50 });
        }

        private static async Task Run()
        {
            var choice = 0;
            while (choice != 10)
            {
                choice = Menu.Display();

                switch (choice)
                {
                    case 1:
                        await ShowBalance();
                        break;
                    case 2:
                        await ShowIncome();
                        break;
                    case 3:
                        await ShowStatus();
                        break;
                    case 4:
                        await ShowLastTransactions();
                        break;
                    case 5:
                        await ShowTransactionLogs();
                        break;
                    case 6:
                        await ShowVehicles();
                        break;
                    case 7:
                        await CreateVehicle();
                        break;
                    case 8:
                        await RemoveVehicle();
                        break;
                    case 9:
                        await RefillVehicle();
                        break;
                    case 10:
                        return;
                    default:
                        continue;
                }
            }
        }

        private static async Task RefillVehicle()
        {
            try
            {
                await ShowVehicles();

                Console.Write("Enter vehicle spot: ");
                var input = Console.ReadLine();
                if (int.TryParse(input, out var index))
                {
                    Console.Write("Enter amount: ");
                    input = Console.ReadLine();
                    if (double.TryParse(input, out var amount))
                    {
                        var vehicle = _vehicles.Where((v, i) => i == index).FirstOrDefault();
                        if (vehicle != null)
                        {
                            await _vehiclesClient.RefillBalance(vehicle.Id, amount);
                        }
                        else
                        {
                            Console.WriteLine("Invalid index");
                        }
                    }
                }
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static async Task RemoveVehicle()
        {
            try
            {
                await ShowVehicles();

                Console.Write("Enter vehicle spot: ");
                var input = Console.ReadLine();
                if (int.TryParse(input, out var index))
                {
                    var vehicle = _vehicles.Where((v, i) => i == index).FirstOrDefault();
                    if (vehicle != null)
                    {
                        await _vehiclesClient.Remove(vehicle.Id);
                    }
                    else
                    {
                        Console.WriteLine("Invalid index");
                    }
                }
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static async Task CreateVehicle()
        {
            var i = 0;
            foreach (var type in Enum.GetValues(typeof(VehicleType)))
            {
                Console.WriteLine($"{i} - {type}");
                i++;
            }

            Console.Write("Enter vehicle type: ");
            var input = Console.ReadLine();
            if (Enum.TryParse<VehicleType>(input, out var vehicleType))
            {
                Console.Write("Enter vehicle starting balance: ");
                input = Console.ReadLine();
                if (int.TryParse(input, out var balance))
                {
                    var vehicle = new Vehicle { Type = vehicleType, Balance = balance };
                    await _vehiclesClient.Create(vehicle);
                }
            }
        }

        private static async Task ShowVehicles()
        {
            Console.WriteLine("Parked vehicles:");

            _vehicles = await _vehiclesClient.GetAll();
            if (!_vehicles.Any())
            {
                Console.WriteLine("There are no vehicles.");
            }
            else
            {
                var i = 0;
                foreach (var vehicle in _vehicles)
                {
                    Console.WriteLine($"{i,2} - {vehicle}");
                    i++;
                }
            }
        }

        private static async Task ShowTransactionLogs()
        {
            try
            {
                Console.WriteLine("Transaction logs:");
                var logs = await _logsClient.GetAll();
                foreach (var log in logs)
                {
                    Console.WriteLine(log);
                }
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static async Task ShowLastTransactions()
        {
            Console.WriteLine("Transactions for the last minute:");
            var transactions = await _transactionsClient.GetAll(TimeSpan.FromMinutes(1).TotalMilliseconds);
            foreach (var transaction in transactions)
            {
                Console.WriteLine(transaction);
            }
        }

        private static async Task ShowStatus()
        {
            var status = await _vehiclesClient.GetStatus();

            Console.WriteLine($"Occupied spots:{status.Taken}, free spots:{status.Free}");

        }

        private static async Task ShowIncome()
        {
            var income = await _transactionsClient.GetBalance(TimeSpan.FromMinutes(1).TotalMilliseconds);
            Console.WriteLine($"Income for the last minute:{income}");
        }

        private static async Task ShowBalance()
        {
            var balance = await _transactionsClient.GetBalance();
            Console.WriteLine($"Balance:{balance}");
        }
    }
}