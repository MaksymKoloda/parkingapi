﻿using System;

namespace Parking.Client
{
    public static class Menu
    {
        public static int Display()
        {
            Console.WriteLine("Parking Lot");
            Console.WriteLine();
            Console.WriteLine(" 1. View balance");
            Console.WriteLine(" 2. View income for the last minute");
            Console.WriteLine(" 3. View number of free and occupied spots");
            Console.WriteLine(" 4. View transactions for the last minute");
            Console.WriteLine(" 5. View transaction logs");
            Console.WriteLine(" 6. View parked vehicles");
            Console.WriteLine(" 7. Add vehicle");
            Console.WriteLine(" 8. Remove vehicle");
            Console.WriteLine(" 9. Refill vehicle balance");
            Console.WriteLine("10. Exit");
            var choice = Console.ReadLine();

            Console.Clear();
            return int.TryParse(choice, out var result) ? result : 0;
        }
    }
}