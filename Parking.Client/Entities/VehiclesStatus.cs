﻿namespace Parking.Client.Entities
{
    public class VehiclesStatus
    {
        public int Taken { get; set; }

        public int Free { get; set; }
    }
}