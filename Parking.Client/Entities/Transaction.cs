﻿using System;

namespace Parking.Client.Entities
{
    public class Transaction
    {
        public Guid Id { get; set; }

        public Guid VehicleId { get; set; }

        public DateTime Time { get; set; }

        public double Bill { get; set; }

        public override string ToString() => $"Time:{Time}; Vehicle:{VehicleId}; Bill:{Bill}";
    }
}