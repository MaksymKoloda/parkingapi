﻿namespace Parking.Client.Entities
{
    public enum VehicleType
    {
        Car,
        Truck,
        Bus,
        Motorcycle
    }
}