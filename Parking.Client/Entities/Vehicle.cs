﻿using System;

namespace Parking.Client.Entities
{
    public class Vehicle
    {
        public Guid Id { get; set; }

        public double Balance { get; set; }

        public VehicleType Type { get; set; }
        
        public override string ToString()
        {
            return $"Id:{Id}; Type:{Type}; Balance:{Balance}";
        }
    }
}