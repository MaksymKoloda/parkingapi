﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Parking.Client.Entities;
using RestEase;

namespace Parking.Client.ApiInterfaces
{
    public interface IVehiclesApi
    {
        [Get]
        Task<IEnumerable<Vehicle>> GetAll();

        [Get("status")]
        Task<VehiclesStatus> GetStatus();

        [Post]
        Task<bool> Create([Body] Vehicle vehicle);

        [Patch("{vehicleId}")]
        Task RefillBalance([Path] Guid vehicleId, [Body] double amount);

        [Delete("{vehicleId}")]
        Task Remove([Path] Guid vehicleId);
    }
}
