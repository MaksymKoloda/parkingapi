﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RestEase;

namespace Parking.Client.ApiInterfaces
{
    public interface ILogsApi
    {
        [Get]
        Task<IEnumerable<string>> GetAll();
    }
}