﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Parking.Client.Entities;
using RestEase;

namespace Parking.Client.ApiInterfaces
{
    public interface ITransactionsApi
    {
        [Get]
        Task<IEnumerable<Transaction>> GetAll([Query] double ticks);

        [Get("balance")]
        Task<double> GetBalance(double ticks = default);
    }
}