﻿using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Parking.Interfaces;
using Parking.Repositories;
using Parking.Services;
using Swashbuckle.AspNetCore.Swagger;

namespace ParkingApi
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddFluentValidation(c =>
                {
                    c.RegisterValidatorsFromAssemblyContaining<Startup>();
                    c.LocalizationEnabled = false;
                });

            services.AddTransient<IVehicleService, VehicleService>();
            services.AddTransient<ITransactionService, TransactionService>();
            services.AddTransient<ITransactionScheduler, TransactionScheduler>();
            services.AddTransient<ILogService, TransactionsLogService>();
            services.AddSingleton<IVehicleRepository, VehicleRepository>();
            services.AddSingleton<ITransactionRepository, TransactionRepository>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "ParkingApi", Version = "v1" });
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ParkingApi"));

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
