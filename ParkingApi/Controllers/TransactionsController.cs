﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Parking.Dtos;
using Parking.Interfaces;

namespace ParkingApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly ITransactionService _transactionService;

        public TransactionsController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TransactionResponseDto>> GetAll(double ticks)
        {
            var transactions = _transactionService.GetTransactions(ticks);

            return transactions.ToList();
        }

        [HttpGet("balance")]
        public ActionResult<double> GetBalance(double ticks = default)
        {
            return _transactionService.GetBalance(ticks);
        }
    }
}