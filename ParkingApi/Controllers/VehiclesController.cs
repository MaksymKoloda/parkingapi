﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Parking;
using Parking.Dtos;
using Parking.Entities;
using Parking.Interfaces;

namespace ParkingApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IVehicleService _vehicleService;
        private readonly ITransactionScheduler _transactionScheduler;

        public VehiclesController(IVehicleService vehicleService, ITransactionScheduler transactionScheduler)
        {
            _vehicleService = vehicleService;
            _transactionScheduler = transactionScheduler;
        }

        [HttpGet]
        public ActionResult<IEnumerable<VehicleResponseDto>> GetAll()
        {
            var result = _vehicleService.GetVehicles();

            return result.ToList();
        }

        [HttpGet("status")]
        public ActionResult<VehiclesStatus> GetStatus()
        {
            var status = _vehicleService.GetStatus();

            return status;
        }

        [HttpPost]
        public ActionResult<bool> Post([FromBody] VehicleRequestDto request)
        {
            var vehicle = new Vehicle(request.Type, request.Balance);

            var added = _vehicleService.AddVehicle(vehicle);
            if (added)
            {
                _transactionScheduler.StartTransactions(vehicle,
                    TimeSpan.FromSeconds(Configuration.TransactionInterval).TotalMilliseconds);
            }

            return added;
        }

        [HttpPatch("{vehicleId}")]
        public IActionResult Patch(Guid vehicleId, [FromBody] double amount)
        {
            _vehicleService.AddToBalance(vehicleId, amount);

            return Ok();
        }

        [HttpDelete("{vehicleId}")]
        public ActionResult<Guid> Delete(Guid vehicleId)
        {
            _vehicleService.RemoveVehicle(vehicleId);

            _transactionScheduler.EndTransactions(vehicleId);

            return vehicleId;
        }
    }
}
