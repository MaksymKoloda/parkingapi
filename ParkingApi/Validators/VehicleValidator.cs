﻿using FluentValidation;
using Parking.Dtos;

namespace ParkingApi.Validators
{
    public class VehicleValidator : AbstractValidator<VehicleRequestDto>
    {
        public VehicleValidator()
        {
            RuleFor(v => v.Type).IsInEnum();
            RuleFor(v => v.Balance).GreaterThan(0);
        }
    }
}
